package org.fryske_akademy.pos_tagger;

/*-
 * #%L
 * udpipe-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

public class RconnectionFactory extends BasePooledObjectFactory<WrappedRConnection> {

    public static final String INPUT_R_VAR_NAME = "text";
    public static final String FORMAT_R_VAR_NAME = "output";
    public static final String UDMODEL_R_VAR_NAME = "udmodel";

    private final String loadScript, udpipeModel;

    public RconnectionFactory(String loadScript, String udpipeModel) {
        this.loadScript = loadScript;
        this.udpipeModel=udpipeModel;
    }

    @Override
    public WrappedRConnection create() throws Exception {
        WrappedRConnection wrappedRConnection = new WrappedRConnection(loadScript);
        wrappedRConnection.assign(UDMODEL_R_VAR_NAME,udpipeModel);
        return wrappedRConnection;
    }

    @Override
    public PooledObject<WrappedRConnection> wrap(WrappedRConnection rConnection) {
        return new DefaultPooledObject<>(rConnection);
    }

    @Override
    public boolean validateObject(PooledObject<WrappedRConnection> p) {
        return p.getObject().isConnected();
    }

    @Override
    public void destroyObject(PooledObject<WrappedRConnection> p) throws Exception {
        p.getObject().deleteOutput();
        p.getObject().close();
    }

    @Override
    public void passivateObject(PooledObject<WrappedRConnection> p) throws Exception {
        p.getObject().deleteOutput();
        p.getObject().assign(INPUT_R_VAR_NAME,"");
    }


}
