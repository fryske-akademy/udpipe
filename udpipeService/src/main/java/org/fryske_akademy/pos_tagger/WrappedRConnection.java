package org.fryske_akademy.pos_tagger;

/*-
 * #%L
 * udpipe-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * RConnection that use temp file to write output to
 */
public class WrappedRConnection extends RConnection {
    public static final String OUTPUT_R_VAR_NAME = "annotated";
    public static final String LOAD_ERRORS = "loaderrors";

    private File output;
    private File loadErrors;

    private final String loadScript;
    public WrappedRConnection(String loadScript) throws RserveException {
        this.loadScript=loadScript;
    }

    public boolean deleteOutput() {
        return output.delete();
    }


    private boolean init;
    private void init() {
        if (init) return;
        try {
            output = File.createTempFile(OUTPUT_R_VAR_NAME, "txt");
            loadErrors = File.createTempFile(LOAD_ERRORS, "log");
            output.deleteOnExit();
            loadErrors.deleteOnExit();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        try {
            assign(LOAD_ERRORS, loadErrors.getPath());
            eval("source(\"" + loadScript + "\")");
        } catch (REngineException e) {
            try {
                throw new IllegalStateException( "Load failed, you may need to use cat \"/proc/PID of Rserve/fd/2\" to find what is wrong. " + new String(Files.readAllBytes(loadErrors.toPath())), e);
            } catch (IOException ioException) {
            }
        }

        try {
            assign(OUTPUT_R_VAR_NAME, output.getPath());
        } catch (RserveException e) {
            throw new IllegalStateException(e);
        }
        init=true;
    }
    public File initOutput() throws IOException {
        init();
        output.createNewFile();
        return output;
    }
}
