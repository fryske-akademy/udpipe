package org.fryske_akademy.pos_tagger;

/*-
 * #%L
 * udpipe-service
 * %%
 * Copyright (C) 2020 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.vectorprint.configuration.cdi.Property;
import org.rosuda.REngine.Rserve.RserveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.json.Json;
import jakarta.json.JsonObject;
import jakarta.servlet.ServletContext;
import jakarta.validation.constraints.Size;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Locale;
import java.util.Properties;
import java.util.UUID;

@ApplicationScoped
@Path(UDPipePostTagService.ROOT)
public class UDPipePostTagService {

    public static final String ROOT = "process";
    public static final String TSVPATH = "tsv";
    public static final String CONLLUPATH = "conllu";
    public static final String INFOPATH = "info";
    private static final Logger LOGGER = LoggerFactory.getLogger(UDPipePostTagService.class);
    public static final String SCRIPTDIR = "/resources";
    private static final String SERVICE_R = "service.R";
    private static final int MAX = 2048000;
    public enum FORMAT {TSV, CONLLU}

    @Context
    private ServletContext servletContext;

    @Inject
    @Property
    private String languageCode;
    @Inject
    private RConnectionPool rConnectionPool;

    private File annotate(String text, FORMAT format) throws Exception {
        String scriptDir = servletContext.getRealPath(SCRIPTDIR);
        WrappedRConnection rConnection = rConnectionPool.borrow();
        try {
            File tempFile = new File(UUID.randomUUID().toString());
            File annotated = rConnection.initOutput();
            try {
                rConnection.assign(RconnectionFactory.FORMAT_R_VAR_NAME, format.name().toLowerCase(Locale.ROOT));
                rConnection.assign(RconnectionFactory.INPUT_R_VAR_NAME, text);
            /*
            To debug script failure server side:
            - lookup process id of Rserve
            - cat /proc/PID/fd/2 (or 1 for stdout), leave open
            - then do request
            */
                rConnection.eval("source(\"" + scriptDir + File.separator + SERVICE_R + "\")");
                Files.move(annotated.toPath(), tempFile.toPath());
                return tempFile;
            } catch (RserveException | IOException ex) {
                tempFile.delete();
                throw ex;
            }
        } finally {
            rConnectionPool.putBack(rConnection);
        }
    }

    @GET
    @Path(INFOPATH)
    @Produces({MediaType.APPLICATION_JSON})
    public Response info() {
        JsonObject jsonObject = Json.createObjectBuilder()
                .add("version",versionInfo())
                .add("languageCode", languageCode)
                .add("udpipeModel",rConnectionPool.getUdpipeModel())
                .add("maxTextSize", MAX)
                .add(INFOPATH, "/"+RestApplication.ROOT+"/"+ROOT+"/"+ INFOPATH)
                .add(TSVPATH, "/"+RestApplication.ROOT+"/"+ROOT+"/"+ TSVPATH)
                .add(CONLLUPATH, "/"+RestApplication.ROOT+"/"+ROOT+"/"+ CONLLUPATH)
                .build();
        return Response.ok(jsonObject).build();
    }

    @POST
    @Path(TSVPATH)
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces("text/tab-separated-values")
    public Response tagTsv(@Size(min=0,max=MAX) String text) {
        return getResponse(text, FORMAT.TSV);
    }

    @POST
    @Path(CONLLUPATH)
    @Consumes({MediaType.TEXT_PLAIN})
    @Produces(MediaType.TEXT_PLAIN)
    public Response tagConllu(@Size(min=0,max=MAX) String text) {
        return getResponse(text, FORMAT.CONLLU);
    }

    private Response getResponse(String text, FORMAT format) {
        try {
            final StreamingOutput output = new StreamingOutput() {
                @Override
                public void write(OutputStream output) throws IOException, WebApplicationException {
                    try {
                        File tempFile = annotate(text, format);
                        try {
                            Files.copy(tempFile.toPath(), output);
                        } finally {
                            tempFile.delete();
                        }
                    } catch (Exception ex) {
                        LOGGER.warn("unable to annotate",ex);
                        Throwable t = deepestCause(ex);
                        throw new WebApplicationException(
                                Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                                   t instanceof RserveException ? "Please retry" : t.getMessage()).build());
                    }
                }
            };
            return Response.ok(output).build();
        } catch (WebApplicationException ex) {
            throw ex;
        } catch (Exception ex) {
            Throwable t = deepestCause(ex);
            throw new WebApplicationException(
                    Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),
                            t instanceof RserveException ? "Please retry" : t.getMessage()).build());
        }
    }

    public static Throwable deepestCause(Throwable t) {
        if (t.getCause() != null) {
            return deepestCause(t.getCause());
        }
        return t;
    }

    private static final Properties BUILDPROPERTIES = new Properties();

    static {
        try {
            BUILDPROPERTIES.load(UDPipePostTagService.class.getResourceAsStream("/build.properties"));
        } catch (IOException e) {
            LOGGER.warn("unable to load build properties",e);
        }
    }

    public String versionInfo() {
        return BUILDPROPERTIES.toString();
    }

}
