## description

UDPipe Frysk is a web app for lemmatizing, PoS tagging and dependency parsing of Frisian texts.

The lemmatizer/tagger/parser are trained on Frisian text that is annotated according to the guidelines of Universal Dependencies version 2.

## building

`mvn verify`

## running

deploy to a jakarta ee 10 compatible app server
see [docker setup](../docker)